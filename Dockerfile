FROM alpine:latest

RUN apk update && \
	apk add --no-cache \
	apache2 \
	php7-apache2 \
	php7 \
	php7-curl \
	php7-xml \
	php7-dom \
	php7-session \
	php7-simplexml \
	php7-xmlreader \
	php7-pdo_sqlite \
	unzip

COPY people.opml /var/www/localhost/htdocs/custom.opml

RUN cd /var/www/localhost/htdocs/ && \
	wget -q https://github.com/moonmoon/moonmoon/releases/download/9.0.0-rc.3/moonmoon-9.0.0-rc.3.zip && \
	unzip -qq moonmoon-9.0.0-rc.3.zip && \
	rm -f index.html && \
	mv moonmoon/* . && rm -rf moonmoon*.zip && \
	sed -i '/\/custom\/people\.opml/d' install.php && \
	touch admin/inc/pwd.inc.php && \
	chown -R apache:apache cache custom admin/inc/pwd.inc.php && \
	rm -rf /var/cache/apk/*

EXPOSE 80

CMD ["httpd", "-D", "FOREGROUND" ]