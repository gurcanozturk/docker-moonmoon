Dockerized version of MoonMoon RSS aggregator.

Build it. 

docker build -t moonmoon .

Run it. (w/ your own RSS links)

docker run -d -p 8010:80 -v /docker/volumes/moonmoon/people.opml:/var/www/localhost/htdocs/custom/people.opml moonmoon